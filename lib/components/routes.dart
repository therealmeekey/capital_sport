import 'package:capital_sport/pages/base/base.dart';
import 'package:capital_sport/pages/digital/digital.dart';
import 'package:capital_sport/pages/finance/finance.dart';
import 'package:capital_sport/pages/home/home.dart';
import 'package:capital_sport/pages/login/login.dart';
import 'package:capital_sport/pages/matchday/matchday.dart';
import 'package:get/get.dart';

class Routes {
  static const String LOGIN = '/login';
  static const String HOME = '/';
  static const String MATCHDAY = '/matchday';
  static const String FINANCE = '/finance';
  static const String BASE = '/base';
  static const String DIGITAL = '/digital';
  //static const String MARKETING = '/marketing';

  static List<GetPage> get aliases => [
        GetPage(name: LOGIN, page: () => LoginPage()),
        GetPage(name: HOME, page: () => HomePage()),
        GetPage(name: MATCHDAY, page: () => MatchdayPage()),
        GetPage(name: FINANCE, page: () => FinancePage()),
        GetPage(name: BASE, page: () => BasePage()),
        GetPage(name: DIGITAL, page: () => DigitalPage()),
      ];
}
