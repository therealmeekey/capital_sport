
import 'dart:ui';

class AppColors {
  // основные цвета
  static Color accentColor = Color(0xFF0072F0);
  static Color primaryColor = Color(0xFF4F5467);
  static Color backgroundColor = Color(0xFFF7F8FC);
  static Color canvasColor = Color(0xFFFFFFFF);
  static Color dividerColor = Color(0xFFEBEFF2);
  //
  // цвета текста
  static Color textColorBlack = Color(0xFF000000);
  static Color textColorWhite = Color(0xFFFFFFFF);
  static Color textColorGray = Color(0xFF4F5467);
  static Color textColorGray2 = Color(0xFF97A0B4);
  static Color textColorLightGray = Color(0xFFB9BBC2);
  static Color textColorGreen = Color(0xFF00C292);
  static Color textColorRed = Color(0xFFE46A76);
}
