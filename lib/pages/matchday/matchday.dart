import 'dart:convert';

import 'package:capital_sport/components/api_config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../main.dart';
import '../../widgets.dart';

class MatchdayPage extends StatefulWidget {
  @override
  _MatchdayPageState createState() => _MatchdayPageState();
}

class _MatchdayPageState extends State<MatchdayPage> {
  List data = [];

  @override
  void initState() {
    getDataMatchday();
    super.initState();
  }

  getDataMatchday() async {
    var token = await storage.read(key: 'token');
    final response = await http.post(ApiConfig.BASE_API_URL + "graph/match-day",
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        });
    if (response.statusCode == 200) {
      Map<String, dynamic> responseBody =
          json.decode(utf8.decode(response.bodyBytes));
      if (mounted) {
        setState(() {
          data.clear();
          data.add(responseBody);
        });
      }
    }
  }

  List<SaleData> getSaleData() {
    List<SaleData> SaleDataList = [];
    for (var i in data[0]['sales_dynamics']) {
      SaleDataList.add(SaleData(i['date'], i['value'].toDouble()));
    }
    return SaleDataList;
  }

  @override
  Widget build(BuildContext context) {
    return UScaffold(
      body: data.length > 0
          ? Column(
              children: [
                SizedBox(height: 20),
                _build1(),
                SizedBox(height: 20),
                _build2(),
                SizedBox(height: 20),
                _build3(),
                SizedBox(height: 20),
                _build5(),
                SizedBox(height: 20),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 2.7,
                ),
                new Center(
                  child: CircularProgressIndicator(),
                )
              ],
            ),
    );
  }

  Widget _build5() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          _buildCard2(title: 'Абонементы', value: data[0]['season_tickets']),
          _buildCard2(title: 'Билеты', value: data[0]['tickets']),
          _buildCard2(title: 'Пригласительные', value: data[0]['invitation']),
        ].separated(SizedBox(width: 20)),
      ),
    );
  }

  Widget _buildCard2({String title, int value}) {
    return Expanded(
      child: Container(
        height: 80,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: AppColors.canvasColor,
          border: Border.all(
            color: AppColors.dividerColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Text(
                title,
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 10,
                  color: AppColors.textColorGray,
                  height: 1.2,
                ),
              ),
            ),
            Text(
              '${value.formatPrice()}',
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
                fontSize: 17,
                color: AppColors.textColorGray,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _build3() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'KPI',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
                Text(
                  data[0]['kpi']['tickets_left'].toString() +
                      ' билетов до выполнения',
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    color: AppColors.textColorGray,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 25),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: ProgressBar(
              percent: (data[0]['kpi']['tickets_left'] * 100) /
                  data[0]['kpi']['tickets_full'],
            ),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Container _build2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Динамика продажи',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 150,
            child: SfCartesianChart(
              plotAreaBorderWidth: 0,
              primaryXAxis: CategoryAxis(
                maximumLabels: 8,
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorLightGray,
                  fontSize: 8,
                ),
                majorGridLines: MajorGridLines(width: 0),
                majorTickLines: MajorTickLines(width: 0),
                axisLine: AxisLine(width: 0),
              ),
              primaryYAxis: NumericAxis(
                isVisible: false,
                maximum: 57,
                axisLine: AxisLine(width: 0),
                majorGridLines: MajorGridLines(width: 0),
                majorTickLines: MajorTickLines(width: 0),
              ),
              series: <ChartSeries<SaleData, String>>[
                ColumnSeries<SaleData, String>(
                  isTrackVisible: false,
                  dataLabelSettings: DataLabelSettings(
                    isVisible: true,
                    labelAlignment: ChartDataLabelAlignment.middle,
                    angle: -90,
                    textStyle: GoogleFonts.poppins(
                      color: AppColors.textColorWhite,
                      fontWeight: FontWeight.w600,
                      fontSize: 10,
                    ),
                  ),
                  dataSource: getSaleData(),
                  width: 0.3,
                  xValueMapper: (SaleData sales, _) => sales.year,
                  yValueMapper: (SaleData sales, _) => sales.sales,
                  color: AppColors.accentColor,
                  borderRadius: BorderRadius.circular(50),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _build1() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          _buildCard(
            title: 'Продано билетов',
            child: Row(
              children: [
                Text(
                  data[0]['tickets_sold']['sold_count'].toString() + '/',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    color: AppColors.textColorGray,
                  ),
                ),
                Text(
                  data[0]['tickets_sold']['max_value'].toString(),
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    color: AppColors.textColorLightGray,
                  ),
                ),
                Spacer(),
                Text(
                  (data[0]['tickets_sold']['sold_count'] == 0
                              ? '0'
                              : (data[0]['tickets_sold']['sold_count'] * 100) /
                                  data[0]['tickets_sold']['max_value'])
                          .toString() +
                      '%',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 13,
                    color: AppColors.textColorGreen,
                  ),
                ),
              ],
            ),
          ),
          _buildCard(
            title: 'На сумму',
            child: Row(
              children: [
                Text(
                  data[0]['sold_sum'].toString(),
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    color: AppColors.textColorGray,
                  ),
                ),
              ],
            ),
          ),
        ].separated(SizedBox(width: 20)),
      ),
    );
  }

  Widget _buildCard({String title, Widget child}) {
    return Expanded(
      child: Container(
        height: 65,
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 10),
        decoration: BoxDecoration(
          color: AppColors.canvasColor,
          border: Border.all(
            color: AppColors.dividerColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Text(
                title,
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 10,
                  color: AppColors.textColorGray,
                  height: 1.2,
                ),
              ),
            ),
            child ?? Container(),
          ],
        ),
      ),
    );
  }
}

class SaleData {
  SaleData(this.year, this.sales);

  final String year;
  final double sales;
}
