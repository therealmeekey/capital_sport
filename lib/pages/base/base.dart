import '../../widgets.dart';
import 'package:flutter_svg/svg.dart';

class BasePage extends StatefulWidget {
  @override
  _BasePageState createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  var _currentDate = DatePickerValue.week;

  @override
  Widget build(BuildContext context) {
    return UScaffold(
      body: Column(
        children: [
          DatePicker(
            value: _currentDate,
            onSelected: (value) => setState(() => _currentDate = value),
          ),
          _build1(),
          SizedBox(height: 20),
          _build2(),
          SizedBox(height: 20),
          _build3(),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Container _build3() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Воронка лояльности',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          SvgPicture.asset(
            'assets/graphic1.svg',
            height: 220,
          ),
        ],
      ),
    );
  }

  Container _build2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Портрет болельщика',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    SizedBox(height: 15),
                    for (int i = 1; i < 5; i++)
                      Text(
                        '$i',
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: AppColors.textColorGray,
                        ),
                      ),
                  ].separated(SizedBox(height: 10)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 7),
                    Text(
                      'Мужчина',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Женщина',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Мужчина',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Мужчина',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                  ].separated(SizedBox(height: 16)),
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Возраст',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      '35-40',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '24-30',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '17-21',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '22-26',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                  ].separated(SizedBox(height: 10)),
                ),
                SizedBox(width: 15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Ср. чек',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      '289₽',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '178₽',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '105₽',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      '166₽',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                  ].separated(SizedBox(height: 10)),
                ),
              ].separated(SizedBox(width: 10)),
            ),
          ),
        ],
      ),
    );
  }

  Container _build1() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Активность базы',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: SvgPicture.asset('assets/base_activity.svg'),
          ),
        ],
      ),
    );
  }
}

class SaleData {
  SaleData(this.year, this.sales);
  final String year;
  final double sales;
}
