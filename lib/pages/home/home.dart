import 'dart:convert';

import 'package:capital_sport/components/api_config.dart';
import 'package:capital_sport/data/network/requests.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';

import '../../main.dart';
import '../../widgets.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _currentDate = DatePickerValue.week;
  List data = [];
  int maxValue = 2000;

  @override
  void initState() {
    getDataHome(_currentDate);
    super.initState();
  }

  List<MatchData> getMatchData() {
    List<MatchData> MatchDataList = [];
    if (data.length > 0 && data[0]['attendance']['matches'] != null) {
      for (var i in data[0]['attendance']['matches']) {
        List<String> parts = i['name'].split(' ');
        parts.remove(parts.removeLast()); // true
        parts.join(' ');
        MatchDataList.add(MatchData(
            i['id'], parts.join(' '), i['date'], i['value'].toDouble()));
      }
    }
    print(maxValue);
    return MatchDataList;
  }

  @override
  Widget build(BuildContext context) {
    return UScaffold(
      body: data.length > 0
          ? Column(
              children: [
                DatePicker(
                  value: _currentDate,
                  onSelected: (value) {
                    setState(() {
                      maxValue = 2000;
                      _currentDate = value;
                    });
                    getDataHome(_currentDate);
                  },
                ),
                _build1(),
                SizedBox(height: 20),
                _build2(),
                SizedBox(height: 20),
                _build3(),
                SizedBox(height: 20),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 2.7,
                ),
                new Center(
                  child: CircularProgressIndicator(),
                )
              ],
            ),
    );
  }

  Widget _build3() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 105,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                color: AppColors.canvasColor,
                border: Border.all(
                  color: AppColors.dividerColor,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      'Доход',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400,
                        fontSize: 10,
                        color: AppColors.textColorGray,
                        height: 1.2,
                      ),
                    ),
                  ),
                  Text(
                    data[0]['income']['value'].toString() + '₽',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                      color: AppColors.textColorGray,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    (data[0]['income']['value'] > 0 ? '+' : '') +
                        data[0]['income']['increase'].toString(),
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w500,
                      fontSize: 11,
                      color: data[0]['income']['increase'] > 0
                          ? AppColors.textColorGreen
                          : AppColors.textColorRed,
                    ),
                  ),
                ],
              ),
            ),
          ),
          _buildCard(
              title: 'База болельщиков',
              value: data[0]['clients']['value'],
              changes: data[0]['clients']['increase']),
          _buildCard(
              title: 'Сайт и соцсети',
              value: data[0]['potential']['value'],
              changes: data[0]['potential']['increase']),
        ].separated(SizedBox(width: 20)),
      ),
    );
  }

  Widget _buildCard({String title, int value, int changes}) {
    return Expanded(
      child: Container(
        height: 105,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: AppColors.canvasColor,
          border: Border.all(
            color: AppColors.dividerColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Text(
                title,
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 10,
                  color: AppColors.textColorGray,
                  height: 1.2,
                ),
              ),
            ),
            Text(
              '${value.formatPrice()}',
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
                fontSize: 17,
                color: AppColors.textColorGray,
              ),
            ),
            SizedBox(height: 5),
            Text(
              (changes > 0 ? '+' : '') + '${changes.formatPrice()}',
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 11,
                color: changes > 0
                    ? AppColors.textColorGreen
                    : AppColors.textColorRed,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _build1() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Средняя заполняемость',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w400,
                    fontSize: 13,
                    color: AppColors.textColorGray,
                  ),
                ),
              ),
              Text(
                data[0]['occupancy']['avg_value'].toString(),
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                  color: AppColors.textColorGreen,
                ),
              ),
              Text(
                '/',
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                  color: AppColors.textColorGray2,
                ),
              ),
              Text(
                data.length > 0 &&
                        data.length > 0 &&
                        data[0]['occupancy']['max_value'].toString() != null
                    ? data[0]['occupancy']['max_value'].toString()
                    : '0',
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w600,
                  fontSize: 15,
                  color: AppColors.textColorGray2,
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          ProgressBar(
            percent: data.length > 0 &&
                    (data[0]['occupancy']['avg_value'] != null ||
                        data[0]['occupancy']['avg_value'] == 0)
                ? (data[0]['occupancy']['avg_value'] * 100) /
                    data[0]['occupancy']['max_value']
                : 0,
          ),
        ],
      ),
    );
  }

  Container _build2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Стоимость проданных билетов',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
                Text(
                  '+',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGreen,
                  ),
                ),
                Text(
                  data.length > 0 &&
                          data[0]['attendance']['increase'].toString() != null
                      ? data[0]['attendance']['increase'].toString()
                      : '0',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGreen,
                  ),
                ),
                Text(
                  '%',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGreen,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 220,
            child: SfCartesianChart(
              enableAxisAnimation: true,
              primaryXAxis: CategoryAxis(
                maximumLabels: 8,
                labelIntersectAction: AxisLabelIntersectAction.rotate90,
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorGray,
                  fontSize: 7,
                  fontWeight: FontWeight.bold,
                ),
                majorGridLines: MajorGridLines(width: 0),
                majorTickLines: MajorTickLines(width: 0),
                axisLine: AxisLine(width: 0),
              ),
              primaryYAxis: NumericAxis(
                labelFormat: '{value}',
                labelPosition: ChartDataLabelPosition.outside,
                labelAlignment: LabelAlignment.start,
                maximum: maxValue.toDouble(),
                labelStyle: GoogleFonts.poppins(
                    color: AppColors.textColorLightGray, fontSize: 9),
                axisLine: AxisLine(width: 0),
                majorGridLines: MajorGridLines(width: 1, dashArray: [5, 5]),
                majorTickLines: MajorTickLines(width: 0),
              ),
              series: <ChartSeries<MatchData, String>>[
                ColumnSeries<MatchData, String>(
                  isTrackVisible: false,
                  dataLabelSettings: DataLabelSettings(
                    isVisible: true,
                    // labelAlignment: ChartDataLabelAlignment.bottom,
                    labelPosition: ChartDataLabelPosition.outside,
                    angle: -90,
                    textStyle: GoogleFonts.poppins(
                      fontSize: 12,
                      color: AppColors.textColorBlack,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  dataSource: getMatchData(),
                  width: 0.5,
                  xValueMapper: (MatchData sales, _) =>
                      sales.name + ' ' + sales.date,
                  yValueMapper: (MatchData sales, _) => sales.value,
                  pointColorMapper: (MatchData sales, _) =>
                      AppColors.textColorGreen,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  getDataHome(currentDate) async {
    try {
      var token = await storage.read(key: 'token');
      String dateFrom;
      String dateTo;
      final DateFormat serverFormater = DateFormat('dd.MM.yyyy');
      var now = new DateTime.now();
      if (currentDate == DatePickerValue.week) {
        dateFrom = serverFormater
            .format(DateTime.now()
                .subtract(Duration(days: DateTime.now().weekday - 1)))
            .toString();
        dateTo = serverFormater
            .format(
                DateTime.now().add(Duration(days: DateTime.now().weekday - 1)))
            .toString();
      } else if (currentDate == DatePickerValue.month) {
        dateFrom =
            serverFormater.format(DateTime(now.year, now.month, 1)).toString();
        dateTo = serverFormater
            .format(DateTime(now.year, now.month + 1, 0))
            .toString();
      } else if (currentDate == DatePickerValue.season) {
        dateFrom = await storage.read(key: 'season_start');
        dateTo = await storage.read(key: 'season_end');
      } else {
        dateFrom = serverFormater.format(DateTime(now.year, 1, 1)).toString();
        dateTo = serverFormater.format(DateTime(now.year, 12, 31)).toString();
      }
      final response = await http.post(
          ApiConfig.BASE_API_URL +
              "graph/avg-occupancy?date_from=" +
              dateFrom.toString() +
              "&date_to=" +
              dateTo.toString(),
          headers: {
            "Authorization": "Bearer $token",
            "Content-Type": "application/json"
          });
      print(response.statusCode);
      // print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseBody =
            json.decode(utf8.decode(response.bodyBytes));
        if (mounted) {
          if (responseBody.length > 0 && responseBody['attendance']['matches'] != null) {
            for (var i in responseBody['attendance']['matches']) {
              setState(() {
                if (maxValue < i['value']) {
                  maxValue = i['value'];
                }
              });
            }
          }
          setState(() {
            data.clear();
            data.add(responseBody);
          });
        }
        print(data);
      }
    } catch (e) {
      print(e);
    }
  }
}

class MatchData {
  MatchData(this.id, this.name, this.date, this.value);

  final int id;
  final String name;
  final String date;
  final double value;
}
