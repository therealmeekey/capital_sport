import 'package:capital_sport/components/api_config.dart';
import 'package:capital_sport/data/api_repository_impl.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/svg.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http_client_with_interceptor.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../../main.dart';
import '../../widgets.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType { login, register }

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  String _email = "";
  String _password = "";
  bool _login_error = false;

  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();

  _LoginPageState() {
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  @override
  void initState() {
    _isAuth();
    super.initState();
  }

  @override
  void dispose() {
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    super.dispose();
  }

  void _requestEmailFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(emailFocusNode);
    });
  }

  void _requestPasswordFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(passwordFocusNode);
    });
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        padding: EdgeInsets.all(16.0),
        child: new GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                SizedBox(
                  height: (MediaQuery.of(context).size.height) * 0.2,
                ),
                _buildIntroText(),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) * 0.1,
                ),
                _buildTextFields(),
                _buildButtons(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildIntroText() {
    return new Container(
      child: new Column(
        children: <Widget>[
          SizedBox(
            height: 82,
            width: 185,
            child: SvgPicture.asset(
              'assets/capital_sport.svg',
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextFields() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            child: new TextField(
              focusNode: emailFocusNode,
              keyboardType: TextInputType.emailAddress,
              onTap: _requestEmailFocus,
              controller: _emailFilter,
              decoration: InputDecoration(
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(3.0),
                    ),
                    borderSide:
                        const BorderSide(color: Color(0xEEEEEE), width: 1.0),
                  ),
                  fillColor: Color(0xffffff),
                  labelText: 'E-mail',
                  labelStyle: GoogleFonts.poppins(
                      fontSize: 14,
                      letterSpacing: 0.2,
                      fontWeight: FontWeight.w400),
                  filled: true),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.only(
              left: 10,
              top: 14,
              right: 10,
            ),
            child: TextField(
              focusNode: passwordFocusNode,
              onTap: _requestPasswordFocus,
              obscureText: true,
              controller: _passwordFilter,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(3.0),
                    ),
                    borderSide: new BorderSide(color: Colors.red, width: 1.0),
                  ),
                  fillColor: Color(0xffffff),
                  labelText: 'Пароль',
                  labelStyle: GoogleFonts.poppins(
                      fontSize: 14,
                      letterSpacing: 0.2,
                      fontWeight: FontWeight.w400),
                  filled: true),
            ),
          ),
          if (_login_error)
            SizedBox(
              height: 15,
            ),
          if (_login_error)
            new Padding(
                padding: const EdgeInsets.only(
                  left: 30,
                  right: 10,
                ),
                child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Text('Неверный логин или пароль, проверьте правильность ввода',
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              color: Colors.grey, fontSize: 10)),
                    ))),
        ],
      ),
    );
  }

  Widget _buildButtons() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(
              left: 10,
              top: 15,
              right: 10,
            ),
            child: SizedBox(
                width: double.infinity, // match_parent
                child: new RaisedButton(
                  child: Text('Войти',
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0.2,
                      )),
                  onPressed: _logIn,
                  color: Color.fromRGBO(0, 114, 240, 1),
                  textColor: Colors.white,
                  splashColor: Colors.grey,
                  padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0)),
                )),
          ),
          if (_login_error)
            SizedBox(
              height: 22,
            ),
          if (_login_error)
            TextButton(
                onPressed: () {
                  _launchURL();
                },
                child: Text('Забыли пароль или не можете войти?',
                    textAlign: TextAlign.left,
                    style:
                        GoogleFonts.poppins(color: Colors.blue, fontSize: 14))),
        ],
      ),
    );
  }

  _launchURL() async {
    const url = 'https://capital-system.com/site/password-reset';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Future<void> _isAuth() async {
    var token = await storage.read(key: 'token');
    if (token != null) {
      Navigator.pushReplacementNamed(context, Routes.HOME);
    }
  }

  void _logIn() async {
    final response = await http.post(
      ApiConfig.BASE_API_URL + "site/authorize/",
      body: {
        'username': _email,
        'password': _password,
      },
    );
    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);
      saveData(responseJson);
      Navigator.pushReplacementNamed(context, Routes.HOME);
    } else {
      if (mounted) {
        setState(() {
          _login_error = true;
        });
      }
    }
  }
}
