import 'dart:convert';

import 'package:capital_sport/components/api_config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';

import '../../widgets.dart';
import '../../main.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;

class FinancePage extends StatefulWidget {
  @override
  _FinancePageState createState() => _FinancePageState();
}

class _FinancePageState extends State<FinancePage> {
  var _currentDate = DatePickerValue.week;
  List data = [];

  @override
  void initState() {
    getDataFinance(_currentDate);
    super.initState();
  }

  getDataFinance(currentDate) async {
    var token = await storage.read(key: 'token');
    String dateFrom;
    String dateTo;
    final DateFormat serverFormater = DateFormat('dd.MM.yyyy');
    var now = new DateTime.now();
    if (currentDate == DatePickerValue.week) {
      dateFrom = serverFormater
          .format(DateTime.now()
              .subtract(Duration(days: DateTime.now().weekday - 1)))
          .toString();
      dateTo = serverFormater
          .format(
              DateTime.now().add(Duration(days: DateTime.now().weekday - 1)))
          .toString();
    } else if (currentDate == DatePickerValue.month) {
      dateFrom =
          serverFormater.format(DateTime(now.year, now.month, 1)).toString();
      dateTo = serverFormater
          .format(DateTime(now.year, now.month + 1, 0))
          .toString();
    } else if (currentDate == DatePickerValue.season) {
      dateFrom = await storage.read(key: 'season_start');
      dateTo = await storage.read(key: 'season_end');
    } else {
      dateFrom = serverFormater.format(DateTime(now.year, 1, 1)).toString();
      dateTo = serverFormater.format(DateTime(now.year, 12, 31)).toString();
    }
    final response = await http.post(
        ApiConfig.BASE_API_URL + "graph/finance?date_from=" + dateFrom.toString() + "&date_to=" + dateTo.toString(),
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        });
    if (response.statusCode == 200) {
      Map<String, dynamic> responseBody =
          json.decode(utf8.decode(response.bodyBytes));
      if (mounted) {
        setState(() {
          data.clear();
          data.add(responseBody);
        });
      }
    }
  }

  List<SaleData> getSaleData() {
    List<SaleData> SaleDataList = [];
    for (var i in data[0]['proceeds']['data']) {
      SaleDataList.add(SaleData(i['name'], i['value'].toDouble()));
    }
    return SaleDataList;
  }

  List<SaleData> getSaleDataBuild2() {
    List<SaleData> SaleDataList = [];
    for (var i in data[0]['average_check']['data']) {
      SaleDataList.add(SaleData(i['name'], i['value'].toDouble()));
    }
    return SaleDataList;
  }

  @override
  Widget build(BuildContext context) {
    return UScaffold(
      body: data.length > 0
          ? Column(
              children: [
                DatePicker(
                  value: _currentDate,
                  onSelected: (value) {
                    setState(() {
                      _currentDate = value;
                    });
                    getDataFinance(_currentDate);
                  },
                ),
                _build1(),
                SizedBox(height: 20),
                _build2(),
                SizedBox(height: 20),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 2.7,
                ),
                new Center(
                  child: CircularProgressIndicator(),
                )
              ],
            ),
    );
  }

  Container _build2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        'Средний чек',
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: AppColors.textColorGray,
                          letterSpacing: 0.2,
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(data[0]['average_check']['sum'].toString() + '₽',
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                          color: AppColors.textColorLightGray,
                          letterSpacing: 0.2,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  '+' + data[0]['average_check']['percentage'].toString() + '%',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGreen,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 220,
            child: SfCartesianChart(
              plotAreaBorderWidth: 0,
              primaryXAxis: CategoryAxis(
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorLightGray,
                  fontSize: 10,
                ),
                majorGridLines: MajorGridLines(width: 0),
                majorTickLines: MajorTickLines(width: 0),
                axisLine: AxisLine(width: 0),
              ),
              primaryYAxis: NumericAxis(
                labelFormat: '{value}₽',
                maximum: 1500,
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorLightGray,
                ),
                axisLine: AxisLine(width: 0),
                majorGridLines: MajorGridLines(width: 1, dashArray: [5, 5]),
                majorTickLines: MajorTickLines(width: 0),
              ),
              series: [
                LineSeries<SaleData, String>(
                  markerSettings: MarkerSettings(
                    borderColor: AppColors.textColorGreen,
                    isVisible: true,
                  ),
                  dataLabelSettings: DataLabelSettings(
                    isVisible: true,
                    labelAlignment: ChartDataLabelAlignment.auto,
                    textStyle: GoogleFonts.poppins(
                      color: AppColors.textColorGray,
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                    ),
                  ),
                  dataSource: getSaleDataBuild2(),
                  width: 2.5,
                  xValueMapper: (SaleData sales, _) => sales.year,
                  yValueMapper: (SaleData sales, _) => sales.sales,
                  color: AppColors.textColorGreen,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _build1() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Выручка от продажи билетов',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
                Text(
                  '+' + data[0]['proceeds']['sum'].toString(),
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGreen,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 150,
            child: SfCartesianChart(
              plotAreaBorderWidth: 0,
              primaryXAxis: CategoryAxis(
                maximumLabels: 8,
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorLightGray,
                  fontSize: 10,
                ),
                majorGridLines: MajorGridLines(width: 0),
                majorTickLines: MajorTickLines(width: 0),
                axisLine: AxisLine(width: 0),
              ),
              primaryYAxis: NumericAxis(
                labelStyle: GoogleFonts.poppins(
                  color: AppColors.textColorLightGray,
                  fontSize: 10,
                ),
                maximum: 234543,
                maximumLabels: 4,
                axisLine: AxisLine(width: 0),
                majorGridLines: MajorGridLines(width: 1, dashArray: [5, 5]),
                majorTickLines: MajorTickLines(width: 0),
              ),
              series: <ChartSeries<SaleData, String>>[
                BarSeries<SaleData, String>(
                  isTrackVisible: false,
                  dataLabelSettings: DataLabelSettings(
                    isVisible: true,
                    labelAlignment: ChartDataLabelAlignment.middle,
                    textStyle: GoogleFonts.poppins(
                      color: AppColors.textColorWhite,
                      fontWeight: FontWeight.w600,
                      fontSize: 10,
                    ),
                  ),
                  dataSource: getSaleData(),
                  width: 0.4,
                  xValueMapper: (SaleData sales, _) => sales.year,
                  yValueMapper: (SaleData sales, _) => sales.sales,
                  color: AppColors.accentColor,
                  borderRadius: BorderRadius.circular(50),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SaleData {
  SaleData(this.year, this.sales);

  final String year;
  final double sales;
}
