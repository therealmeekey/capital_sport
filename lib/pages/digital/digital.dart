import '../../widgets.dart';

class DigitalPage extends StatefulWidget {
  @override
  _DigitalPageState createState() => _DigitalPageState();
}

class _DigitalPageState extends State<DigitalPage> {
  var _currentDate = DatePickerValue.week;

  @override
  Widget build(BuildContext context) {
    return UScaffold(
      body: Column(
        children: [
          DatePicker(
            value: _currentDate,
            onSelected: (value) => setState(() => _currentDate = value),
          ),
          _build1(),
          SizedBox(height: 20),
          _build2(),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Container _build2() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Icon(
                  MdiIcons.web,
                  color: AppColors.textColorGray,
                ),
                SizedBox(width: 10),
                Text(
                  'Официальный сайт',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    color: AppColors.textColorGray,
                    letterSpacing: 0.2,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Просмотры',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      13456.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                  ].separated(SizedBox(height: 11.3)),
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Посетители',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      3332.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                  ].separated(SizedBox(height: 11.3)),
                ),
                SizedBox(width: 15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Прирост',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      '+123',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGreen,
                      ),
                    ),
                  ].separated(SizedBox(height: 11.3)),
                ),
              ].separated(SizedBox(width: 10)),
            ),
          ),
        ],
      ),
    );
  }

  Container _build1() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        border: Border.all(
          color: AppColors.dividerColor,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          SizedBox(height: 5),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Социальные сети',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColors.textColorGray,
                      letterSpacing: 0.2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(
            color: AppColors.dividerColor,
            height: 1,
          ),
          SizedBox(height: 15),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    SizedBox(height: 15),
                    Icon(
                      MdiIcons.vk,
                      color: AppColors.textColorGray,
                    ),
                    Icon(
                      MdiIcons.instagram,
                      color: AppColors.textColorGray,
                    ),
                    Icon(
                      MdiIcons.twitter,
                      color: AppColors.textColorGray,
                    ),
                    Icon(
                      MdiIcons.facebook,
                      color: AppColors.textColorGray,
                    ),
                    Icon(
                      MdiIcons.youtube,
                      color: AppColors.textColorGray,
                    ),
                  ].separated(SizedBox(height: 10)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 6),
                    Text(
                      'Вконтакте',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Instagram',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Twitter',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Facebook',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      'Youtube',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                  ].separated(SizedBox(height: 18.3)),
                ),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Подписчики',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      56866.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      77899.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      15674.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      34444.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                    Text(
                      105647.formatPrice(),
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGray,
                      ),
                    ),
                  ].separated(SizedBox(height: 11.3)),
                ),
                SizedBox(width: 15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Прирост',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 11,
                        color: AppColors.textColorGray2,
                      ),
                    ),
                    Text(
                      '+34',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGreen,
                      ),
                    ),
                    Text(
                      '-4',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorRed,
                      ),
                    ),
                    Text(
                      '-99',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorRed,
                      ),
                    ),
                    Text(
                      '+564',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGreen,
                      ),
                    ),
                    Text(
                      '+955',
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        color: AppColors.textColorGreen,
                      ),
                    ),
                  ].separated(SizedBox(height: 11.3)),
                ),
              ].separated(SizedBox(width: 10)),
            ),
          ),
        ],
      ),
    );
  }
}

class SaleData {
  SaleData(this.year, this.sales);
  final String year;
  final double sales;
}
