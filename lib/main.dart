import 'package:capital_sport/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'controllers/tab_controller.dart';

final storage = FlutterSecureStorage();

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(TabScrollController());
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Capital Sport',
      defaultTransition: Transition.fadeIn,
      transitionDuration: Duration(milliseconds: 100),
      initialRoute: Routes.LOGIN,
      getPages: Routes.aliases,
      theme: ThemeData(primaryColor: AppColors.textColorGreen),
    );
  }
}
