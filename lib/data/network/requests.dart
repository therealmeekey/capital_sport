import 'dart:convert';

import 'package:capital_sport/components/api_config.dart';
import 'package:capital_sport/widgets/date_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:http_interceptor/http_client_with_interceptor.dart';
import 'package:intl/intl.dart';

import '../api_repository_impl.dart';

class Request {
  Client client = HttpClientWithInterceptor.build(interceptors: [
    RequestWithTokenIntereptor(),
  ]);

  // ignore: missing_return
  getDataHome(currentDate) async {
    String dateFrom;
    String dateTo;
    final DateFormat serverFormater = DateFormat('dd.MM.yyyy');
    var now = new DateTime.now();
    if (currentDate == DatePickerValue.week) {
      dateFrom = serverFormater
          .format(DateTime.now()
              .subtract(Duration(days: DateTime.now().weekday - 1)))
          .toString();
      dateTo = serverFormater
          .format(
              DateTime.now().add(Duration(days: DateTime.now().weekday - 1)))
          .toString();
    } else if (currentDate == DatePickerValue.month) {
      dateFrom =
          serverFormater.format(DateTime(now.year, now.month, 1)).toString();
      dateTo = serverFormater
          .format(DateTime(now.year, now.month + 1, 0))
          .toString();
    } else if (currentDate == DatePickerValue.season) {
    } else {
      dateFrom = serverFormater.format(DateTime(now.year, 1, 1)).toString();
      dateTo = serverFormater.format(DateTime(now.year, 12, 31)).toString();
    }
    Map<String, dynamic> jsonString = {
      "date_from": dateFrom,
      "date_to": dateTo,
      "interval_id": "1"
    };
    String body = jsonEncode(jsonString);
    final response = await client
        .post(ApiConfig.BASE_API_URL + "graph/avg-occupancy", body: body);
    if (response.statusCode == 200) {
      try {
        // List responseBody = json.decode(response.body);
        Map<String, dynamic> responseBody =
            json.decode(utf8.decode(response.bodyBytes));
        print(responseBody['occupancy']);
        return responseBody;
      } catch (e) {
        print(e);
      }
    } else {
      print(response.body);
    }
  }
}

logout() async {
  final storage = new FlutterSecureStorage();
  await storage.delete(key: 'token');
  return true;
}
