import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http_interceptor/interceptor_contract.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';

import '../main.dart';

class RequestWithTokenIntereptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    try {
      var token = await storage.read(key: 'token');
      data.headers["Content-Type"] = "application/json";
      data.headers["Authorization"] = "Bearer $token";
    } catch (e) {
      print(e);
    }
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    return data;
  }
}

saveData(Map responseJson) async {
  if ((responseJson != null && !responseJson.isEmpty)) {
    await storage.write(key: 'token', value: responseJson['user']['accesstoken']);
    await storage.write(key: 'username', value: responseJson['user']['username']);
    await storage.write(key: 'season_start', value: responseJson['season_start']);
    await storage.write(key: 'season_end', value: responseJson['season_end']);
  } else {
    print("error write to disk");
  }
}
