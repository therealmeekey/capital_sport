import '../widgets.dart';

class TabScrollController extends GetxController {
  static TabScrollController to = Get.find();

  final _scrollValue = 0.0.obs;

  double get scrollValue => _scrollValue.value;
  set scrollValue(double value) => _scrollValue.value = value;
}
