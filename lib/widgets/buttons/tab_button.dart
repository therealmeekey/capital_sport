import '../../widgets.dart';

class TabButton extends StatelessWidget {
  TabButton({
    @required this.caption,
    @required this.routeName,
  });

  final String caption;
  final String routeName;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:
          routeName == Get.currentRoute ? null : () => Get.toNamed(routeName),
      child: Container(
        decoration: BoxDecoration(
          color: routeName == Get.currentRoute
              ? AppColors.dividerColor
              : AppColors.canvasColor,
          border: Border(
            bottom: BorderSide(
              color: routeName == Get.currentRoute
                  ? AppColors.accentColor
                  : AppColors.canvasColor,
              width: 2,
            ),
          ),
        ),
        padding: EdgeInsets.symmetric(horizontal: 25),
        child: Center(
          child: Text(
            caption,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w400,
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
