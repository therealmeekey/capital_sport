import '../../widgets.dart';

class UIconButton extends StatelessWidget {
  UIconButton({
    @required this.iconData,
    this.iconSize = 26,
    this.padding = const EdgeInsets.all(8),
    this.width = 45,
    this.height = 45,
    this.color,
    this.isOutline = false,
    this.badgeCount = 0,
    @required this.onPressed,
  }) : assert(iconData != null && isOutline != null && badgeCount != null);

  final IconData iconData;
  final double iconSize;
  final EdgeInsets padding;
  final double width;
  final double height;
  final Color color;
  final bool isOutline;
  final int badgeCount;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: onPressed.isNull ? 0.7 : 1,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          border: Border.all(
            color: isOutline
                ? color?.withOpacity(0.1) ?? context.theme.canvasColor
                : Colors.transparent,
            width: 2,
          ),
          shape: BoxShape.circle,
          color: Colors.transparent,
        ),
        child: InkWell(
          splashColor: color?.withAlpha(20),
          hoverColor: color?.withAlpha(17),
          highlightColor: color?.withAlpha(20),
          splashFactory: InkSplash.splashFactory,
          customBorder: CircleBorder(),
          child: Padding(
            padding: padding,
            child: Icon(
              iconData,
              size: iconSize,
              color: color ?? context.theme.accentColor,
            ),
          ),
          onTap: onPressed,
        ),
      ),
    );
  }
}
