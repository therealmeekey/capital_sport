import '../../widgets.dart';

class ToggleButton extends StatelessWidget {
  ToggleButton({
    @required this.caption,
    this.value = false,
    this.onPressed,
  }) : assert(caption != null);

  final String caption;
  final bool value;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(3),
      onTap: onPressed,
      child: AnimatedContainer(
        curve: Curves.ease,
        duration: Duration(milliseconds: 300),
        width: 80,
        decoration: BoxDecoration(
          color: value ? AppColors.accentColor : AppColors.canvasColor,
          border: Border.all(
            color: value ? AppColors.accentColor : AppColors.dividerColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(3),
        ),
        child: Center(
          child: Text(
            caption,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.w400,
              fontSize: 13,
              color: value ? AppColors.textColorWhite : AppColors.textColorGray,
            ),
          ),
        ),
      ),
    );
  }
}
