import 'package:capital_sport/controllers/tab_controller.dart';
import 'package:capital_sport/data/network/requests.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../widgets.dart';
import '../main.dart';

class UScaffold extends StatefulWidget {
  UScaffold({
    @required this.body,
  }) : assert(body != null);

  final Widget body;

  @override
  _UScaffoldState createState() => _UScaffoldState();
}

class _UScaffoldState extends State<UScaffold> {
  ScrollController _controller;
  String username;

  @override
  void initState() {
    getUsername();
    final controller = TabScrollController.to;
    _controller = ScrollController(initialScrollOffset: controller.scrollValue);

    _controller.addListener(() => controller.scrollValue = _controller.offset);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: _buildDrawer(),
        backgroundColor: AppColors.backgroundColor,
        body: Column(
          children: [
            _buildStatusBar(),
            _buildAppBar(),
            _buildTabBar(),
            _buildBody(),
          ],
        ));
  }

  Widget _buildDrawer() {
    return Container(
      color: AppColors.accentColor,
      width: 250,
      height: double.infinity,
      padding: EdgeInsets.only(
        left: 10,
      ),
      child: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              _buildPopupAccount(isMenu: true),
              _buildMenuButton(
                'Главная',
                routeName: Routes.HOME,
              ),
              _buildMenuButton('Matchday', routeName: Routes.MATCHDAY),
              _buildMenuButton('Финансы', routeName: Routes.FINANCE),
              // _buildMenuButton('База', routeName: Routes.BASE),
              // _buildMenuButton('Digital', routeName: Routes.DIGITAL),
              SizedBox(height: 100),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0.0, 0.4],
                  colors: [
                    AppColors.accentColor.withAlpha(0),
                    AppColors.accentColor,
                  ],
                ),
              ),
              padding: EdgeInsets.only(bottom: 10),
              child: Material(
                type: MaterialType.transparency,
                child: InkWell(
                  splashColor: Colors.white12,
                  highlightColor: Colors.white12,
                  borderRadius:
                  BorderRadius.horizontal(left: Radius.circular(50)),
                  onTap: () =>
                  {
                    logout(),
                    Get.offNamed(Routes.LOGIN),
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 18, vertical: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Выйти",
                          style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              // _buildMenuButton("Выйти", routeName: Routes.LOGIN),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMenuButton(String text, {
    String routeName,
    Function onPressed,
  }) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        splashColor: Colors.white12,
        highlightColor: Colors.white12,
        borderRadius: BorderRadius.horizontal(left: Radius.circular(50)),
        onTap: onPressed ?? () => Get.offNamed(routeName),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text,
                style: GoogleFonts.poppins(
                  color: onPressed != null || routeName != null
                      ? Colors.white
                      : Colors.white60,
                  fontSize: 15,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Expanded(
      child: SingleChildScrollView(
        child: widget.body,
      ),
    );
  }

  Widget _buildStatusBar() {
    return Container(
      color: AppColors.accentColor,
      height: Get.mediaQuery.padding.top,
    );
  }

  Widget _buildAppBar() {
    return Builder(
      builder: (context) {
        return SizedBox(
          height: AppConstants.appBarHeight,
          child: Material(
            color: AppColors.accentColor,
            child: Row(
              children: [
                SizedBox(width: 6),
                UIconButton(
                    iconData: Icons.menu,
                    color: AppColors.textColorWhite,
                    onPressed: () => Scaffold.of(context).openDrawer()),
                Spacer(),
                _buildPopupAccount(),
                SizedBox(width: 6),
              ],
            ),
          ),
        );
      },
    );
  }

  getUsername() async {
    var user_name = await storage.read(key: 'username');
    if (mounted) {
      setState(() {
        username = user_name;
      });
    }
  }

  Widget _buildPopupAccount({bool isMenu}) {
    return Padding(
      padding: isMenu == true ? EdgeInsets.all(18) : EdgeInsets.zero,
      child: Row(
        children: [
        SizedBox(
        height: 23,
        child: _buildPopupAccountAvatar(),
      ),
      SizedBox(width: 10),
      Text(
        username==null?'':username,
        style: GoogleFonts.poppins(
        color: AppColors.textColorWhite,
        fontSize: 15,
      ),
    ),
    SizedBox(width: 4),
    // if (isMenu != true)
    //   Icon(
    //     MdiIcons.menuDown,
    //     color: AppColors.textColorWhite,
    //     size: 25,
    //   ),
    // SizedBox(width: 6),
    ],
    ),
    );
  }

  Widget _buildPopupAccountAvatar() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.canvasColor,
        shape: BoxShape.circle,
      ),
      child: Image.asset('assets/logo/club_logo.png'),
    );
  }

  Widget _buildTabBar() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          color: AppColors.canvasColor,
          height: 50,
        ),
        SizedBox(
          height: 50,
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            controller: _controller,
            children: [
              TabButton(caption: 'Главная', routeName: Routes.HOME),
              TabButton(caption: 'Matchday', routeName: Routes.MATCHDAY),
              TabButton(caption: 'Финансы', routeName: Routes.FINANCE),
              // TabButton(caption: 'База', routeName: Routes.BASE),
              // TabButton(caption: 'Digital', routeName: Routes.DIGITAL),
            ],
          ),
        ),
      ],
    );
  }
}
