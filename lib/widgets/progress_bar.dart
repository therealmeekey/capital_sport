import '../widgets.dart';

class ProgressBar extends StatelessWidget {
  ProgressBar({
    this.percent,
  });

  final double percent;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraint) {
        final width = constraint.maxWidth;
        final widthProgress = width * percent / 100;
        return Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.dividerColor,
                borderRadius: BorderRadius.circular(50 * Get.width),
              ),
              height: AppConstants.progressBarHeight,
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 700),
              curve: Curves.easeOutExpo,
              decoration: BoxDecoration(
                color: AppColors.accentColor,
                borderRadius: BorderRadius.circular(50 * Get.width),
              ),
              width: widthProgress < 50 ? 50 : widthProgress,
              height: AppConstants.progressBarHeight,
              child: Center(
                child: Text(
                  '${percent.floor()}%',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    color: AppColors.textColorWhite,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
