import '../widgets.dart';

enum DatePickerValue { year, season, month, week }

class DatePicker extends StatelessWidget {
  DatePicker({
    @required this.value,
    this.onSelected,
  }) : assert(value != null);

  final DatePickerValue value;
  final Function(DatePickerValue) onSelected;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 74,
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(20),
        children: [
          ToggleButton(
            caption: 'Год',
            onPressed: () => onSelected(DatePickerValue.year),
            value: value == DatePickerValue.year,
          ),
          ToggleButton(
            caption: 'Сезон',
            onPressed: () => onSelected(DatePickerValue.season),
            value: value == DatePickerValue.season,
          ),
          ToggleButton(
            caption: 'Месяц',
            onPressed: () => onSelected(DatePickerValue.month),
            value: value == DatePickerValue.month,
          ),
          ToggleButton(
            caption: 'Неделя',
            onPressed: () => onSelected(DatePickerValue.week),
            value: value == DatePickerValue.week,
          ),
          // UIconButton(
          //   iconSize: 30,
          //   padding: EdgeInsets.zero,
          //   iconData: MdiIcons.calendarMonth,
          //   color: AppColors.textColorGray2,
          //   onPressed: () {},
          // ),
        ].separated(SizedBox(width: 8)),
      ),
    );
  }
}
