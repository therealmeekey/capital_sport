import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

extension SeparatedWidgets on List {
  List<Widget> separated(Widget widget) {
    final result = <Widget>[];

    for (var i = 0; i < length; i++) {
      if (i != 0) result.add(widget);
      result.add(this[i]);
    }

    return result;
  }

  List<Widget> ifNull(Widget widget) {
    if (length == 0) add(widget);
    return this;
  }
}

extension FormatInt on int {
  String formatPrice() {
    if (this == null) return "0";
    var numberFormat = new NumberFormat("#,###", "ru_RU");
    return numberFormat.format(this);
  }
}
